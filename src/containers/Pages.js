import React from "react"
import {Route,BrowserRouter,Switch,Router,Link,Redirect} from "react-router-dom"
import Home from "./Home"
import DetailPage from "./DetailPage"
import {ApolloClient} from 'apollo-boost'


/**
var client=new ApolloClient({
   uri:'http://localhost:6500/graphql'
});
*/

class Pages extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <BrowserRouter>
                <div>
                <div>
                    <Link to={"/home"}>To Home Page</Link>
                    <Redirect from={"/home"} to={"/detail"}>FRom Home To Detail</Redirect>
                </div>
                <Switch>
                    <Redirect from={"/home"} to={"/detail"}>FRom Home To Detail</Redirect>
                    <Route path="/home" component={Home}></Route>
                    <Route path={"/detail"} component={DetailPage}></Route>
                 </Switch>
                </div>
            </BrowserRouter>
        )
    }
}
export default Pages
