import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TemparatureCalculator from './App';
import registerServiceWorker from './registerServiceWorker';
import  ReactDOMServer from 'react-dom/server.browser'
import ModalDemo from "./component/Modal"
import Cutomization from "./component/Cutomization"
import AppBAr from "./component/AppBAr";
import AvatarDemo from './component/AvatarDemo'
import Drawer from './component/Drawer'
import Pages from "./containers/Pages"
import MobExTester from './component/MobExTester'
import {observable} from "mobx"
import {observer} from "mobx-react"
import {ApolloClient,HttpLink,InMemoryCache} from "apollo-boost";
import ApolloClientComponent from "./component/ApolloClientComponent";
import gql from "graphql-tag"

var couneter =observable({
    counter:0,
});


couneter.inc=()=>{
    couneter.counter++;
};

couneter.dec=()=>{
    couneter.counter--
};


var client=new ApolloClient({
    link:new HttpLink({
        uri:'http://localhost:8500/graphql'
    }),
    cache:new InMemoryCache()
});

console.log(client);

var tt=client
.query({
    query: gql`
        {
            human{
                name
            }
        }
    `
})
.then(result => console.log(result));
console.log(tt);

//ReactDOM.render(<ModalDemo/>, document.getElementById('modal'));
//ReactDOM.render(<Cutomization/>, document.getElementById('custom'));
//ReactDOM.render(<AppBAr/>, document.getElementById('appbar'));
//ReactDOM.render(<AvatarDemo/>, document.getElementById('avatarDemo'));
//ReactDOM.render(<Drawer/>, document.getElementById('drawer'));
//ReactDOM.render(<MobExTester store={couneter}/>, document.getElementById('mobx'));
//ReactDOM.render(<Pages store={couneter}/>, document.getElementById('page'));
ReactDOM.render(<ApolloClientComponent/>, document.getElementById('apollo'));
registerServiceWorker();


//console.log(ReactDOMServer.renderToString(<TemparatureCalculator/>));
//nsole.log(ReactDOMServer.renderToStaticMarkup(<TemparatureCalculator/>));
