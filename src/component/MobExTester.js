import React from "react"
import {observable} from "mobx"
import {observer} from "mobx-react"
import DevTools from "mobx-react-devtools"

var objectvableArrat=observable.array([3,5,3,25,77,6]);

var observableObject=observable({
    a:"s",
    aa:"sss",
});

var observableMap=observable.map({"key":"Value From Demo Application"});

class MobExTester extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <div>
                <DevTools/>
                <button onClick={this.props.store.inc}>++</button>
                <button onClick={this.props.store.dec}>---</button>
                <p>{this.props.store.counter}</p>
            </div>
        )
    }
}

export default observer(MobExTester)
