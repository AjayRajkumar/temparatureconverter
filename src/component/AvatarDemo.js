import  React from "react"
import  Avatar from "@material-ui/core/Avatar"
import Badge from "@material-ui/core/Badge"
import  {withStyles} from "@material-ui/core/styles"
import Chip from "@material-ui/core/Chip"
import Icon from "@material-ui/core/Icon"
import Card from "@material-ui/core/Card"
import CardHeader from "@material-ui/core/CardHeader"
import CardContent from "@material-ui/core/CardContent"
import CardActions from "@material-ui/core/CardActions"
import CardMedia from "@material-ui/core/CardMedia"
import Typography from "@material-ui/core/Typography"
import Button from "@material-ui/core/Button"

//
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import Divider from "@material-ui/core/Divider"



const style=(theme)=>({
    root:{
        display:'flex',
        justifyContent:'center'
    },
    margin:{
        margin:10,
        backgroundColor:"green",
        color:"white"
    },
    chipStyle:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-around',
    },
    cardStyle:{
        maxWidth:500,
        margin:50,
        border:2,
    },
    content:{
        display:'flex',
        flexDirection:'row',
        flexWrap:'wrap'
    },
    alignRight:{
        alignRight:'true'
    },
    list:{
        maxWidth:400,
        backgroundColor: theme.palette.background.paper
    }

});

class AvatarDemo extends React.Component{
    constructor(props){
        super(props)
    }

    click(event){
        alert("You hav  e Clicked the",event)
    }

    render(){
        console.log(this.props.children);
        return(
            <div>
            <div className={this.props.classes.root}>
                <Avatar  alt={"Welcome To Home Page Again"} className={this.props.classes.margin}
                         >H</Avatar>
                <Badge color={"primary"} badgeContent={4}></Badge>
            </div>
            <div className={this.props.classes.chipStyle}>
                    <Chip label={"Demo Page"} clickable></Chip>
                    <Chip label={"Demo Page"} avatar={<Avatar><Icon>face</Icon></Avatar>}></Chip>
                    <Chip label={"College"} avatar={<Avatar><Icon>face</Icon></Avatar>}></Chip>
                    <Chip onDelete={()=>{}} onClick={this.click} label={"Demo Info"}
                          avatar={<Avatar><Icon>face</Icon></Avatar>}
                          deleteIcon={<Avatar><Icon>close</Icon></Avatar>}></Chip>
              </div>
              <div className={this.props.classes.cardStyle}>
                  <Card className={this.props.classes.cardStyle}>
                      <CardMedia src={"/a.jpg"} title={"Demo"}>

                      </CardMedia>
                      <CardContent>
                          <Typography  color={"primary"} noWrap={true}>
                              Welcome To the Card Form Your Confirmation
                          </Typography>

                          <Typography  color={"secondary"} noWrap={true} variant={"subheading"}>
                              World News
                          </Typography>

                          <Typography  color={"primary"} noWrap={false} variant={"subheading"}>
                              Welcome To The Bustle Info Technology to shoft the entire infrastructure
                              to the world of information Technology
                          </Typography>
                      </CardContent>
                      <CardActions>
                          <Button className={this.props.classes.alignRight} variant={"raised"} color={"primary"} size={"small"}>
                              Book
                          </Button>
                      </CardActions>
                  </Card>

                  <div>
                  <List className={this.props.classes.list}>
                      <ListItem button={"true"}>
                          <Avatar><Icon>face</Icon></Avatar>
                          <ListItemText primary={"Welcome"} secondary={"This is June"}></ListItemText>
                      </ListItem>
                      <Divider inset/>
                      <ListItem button>
                          <Avatar><Icon>contacts</Icon></Avatar>
                          <ListItemText primary={"Infoview"} secondary={"Next is July"}></ListItemText>
                      </ListItem>
                      <Divider/>
                      <ListItem button>
                          <ListItemText primary={"WorksApplication"}></ListItemText>
                      </ListItem>
                      <Divider component={"p"}/>
                  </List>
                  </div>
               </div>
            </div>
        )
    }
}
export default withStyles(style)(AvatarDemo)
