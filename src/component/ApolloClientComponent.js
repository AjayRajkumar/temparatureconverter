import React from "react"
import {ApolloClient,ApolloLink,A} from 'apollo-boost'
import TypoGraphy from "@material-ui/core/Typography"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import ListItemAvatar from "@material-ui/core/ListItemAvatar"
import ListItemIcon from "@material-ui/core/ListItemIcon"
import ReactDOM from "react-dom";
import {ApolloProvider,Query} from "react-apollo"
import {HttpLink, InMemoryCache} from "apollo-boost/lib/index";
import gql from "graphql-tag";
import ApolloComponentDataList from "./ApolloComponentDataList";

var client=new ApolloClient({
    link:new HttpLink({
        uri:'http://localhost:8500/graphql'
    }),
    cache:new InMemoryCache()
});

console.log(client);

var tt=client
.query({
    query: gql`
        {
            human{
                name
            }
        }
    `
})
.then(result => console.log(result));
console.log(tt);


class ApolloClientComponent extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        const renderHello = ({ loading, data: { human } }) => (
            loading ? <p>Loading…</p> :
                <h1> ${human.name}</h1>
        );
        var QUERY_USER=  gql`
            {
                human{
                    name,
                    age
                }
            }
        `
        return(
            <ApolloProvider client={client}>
                <Query query={QUERY_USER}>
                    {({loading,data:{human}})=>(
                        loading ? <TypoGraphy variant={"headline"}>Welcome To GraphQl</TypoGraphy> :
                                <ApolloComponentDataList dataValue={human} key={human.name}/>)
                           }

                </Query>
            </ApolloProvider>
        )
    }
}
export default ApolloClientComponent
