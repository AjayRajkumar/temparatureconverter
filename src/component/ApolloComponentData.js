import React from "react"
import  {withStyles} from "@material-ui/core/styles"
import {ApolloClient,ApolloLink,A} from 'apollo-boost'
import Typography from "@material-ui/core/Typography"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import ListItemAvatar from "@material-ui/core/ListItemAvatar"
import ListItemIcon from "@material-ui/core/ListItemIcon"
import Icon from "@material-ui/core/Icon"
import Divider from "@material-ui/core/Divider"
import ReactDOM from "react-dom";
import {ApolloProvider,Query} from "react-apollo"
import {HttpLink, InMemoryCache} from "apollo-boost/lib/index";
import gql from "graphql-tag";

const style=theme=>({
  root:{
      display:'flex',
      flexDirection:'row',
      flexWrap:'wrap',
      width:'100%',
      maxWidth:'360px'
  },
    align:{
      alignRight:true
    }
});

/**
<Typography variant={"body1"} color={"primary"}>{this.props.name}</Typography>
<Typography variant={"display1"} color={"primary"}>{this.props.age}</Typography>
*/

class ApolloComponentData extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <div className={this.props.classes.root}>
                <List component={"nav"} className={this.props.classes.align}>
                    <ListItem button>
                        <ListItemIcon><Icon>face</Icon></ListItemIcon>
                        <ListItemText primary={this.props.name} secondary={this.props.age}/>
                        <Divider inset/>
                    </ListItem>
                </List>
            </div>
        )
    }
}

export default withStyles(style)(ApolloComponentData)
