import React from "react"
import {ApolloClient,ApolloLink,A} from 'apollo-boost'
import Typography from "@material-ui/core/Typography"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import ListItemAvatar from "@material-ui/core/ListItemAvatar"
import ListItemIcon from "@material-ui/core/ListItemIcon"
import ReactDOM from "react-dom";
import {ApolloProvider,Query} from "react-apollo"
import {HttpLink, InMemoryCache} from "apollo-boost/lib/index";
import gql from "graphql-tag";
import ApolloComponentData from "./ApolloComponentData"

class ApolloComponentDataList extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <div>
                {
                    this.props.dataValue.map((data,i)=>{
                        return (<ApolloComponentData name={data.name} age={data.age} key={data.age}/>)
                    })
                }
            </div>
        )
    }
}


export default ApolloComponentDataList
