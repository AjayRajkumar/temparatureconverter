import React from "react"
import {withStyles,withTheme,createMuiTheme,Theme,MuiThemeProvider} from "@material-ui/core/styles"
import Button from "@material-ui/core/Button"
import red from "@material-ui/core/colors/red"
import indigo from "@material-ui/core/colors/indigo"
import blue from "@material-ui/core/colors/blue"

const muiTheme=createMuiTheme({
   palette:{
       primary:{
           main:blue[800]
       },
       secondary:indigo,
       error:red
   }
});


const styles=(theme)=>({
   root:{
       heigh:200,
       align:'center',
   },
    container:{
        display:'flex',
        paddingLeft:450,
        marginLeft:40,
        textAlign:'center'
    }
});

class Cutomization  extends React.Component{
    constructor(props){
        super(props);
        this.state={

        };
    }
    render(){
        const {classes}=this.props;
        console.log(this.props.theme);
        console.log(muiTheme);
        return(
            <MuiThemeProvider theme={muiTheme}>
            <div>
                <Button>
                    Demo Page
                </Button>
            </div>
            </MuiThemeProvider>
        )
    }
}
export default withTheme()(Cutomization)
