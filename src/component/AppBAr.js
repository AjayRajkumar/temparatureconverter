import  React from "react"
import  {withStyles} from "@material-ui/core/styles"
import  AppBar from "@material-ui/core/AppBar"
import  Toolbar from "@material-ui/core/Toolbar"
import  Typography from "@material-ui/core/Typography"
import IconButton from "@material-ui/core/IconButton"
import Icon from "@material-ui/core/Icon"
import Button from "@material-ui/core/Button"
import Avatar from "@material-ui/core/Avatar"

const style=(theme)=>({
   root:{
       flexGrow:1
   },
    flex:{
       flex:1
    },
    menuButton:{
       marginLeft:-12,
        marginRight:20
    }

});

class AppBAr extends React.Component{
    constructor(props)
    {
        super(props);
        this.state={

        }
    }
    render(){
        const {classes}= this.props;
        return(
            <div className={classes.root}>
                <AppBar position="static" >
                    <Toolbar >
                        <IconButton className={classes.menuButton}  aria-label={"Menu"}>
                            <Icon>menu</Icon>
                        </IconButton>
                        <Typography color={"inherit"} className={classes.flex} variant={"title"}>
                            Welome To the Material Ui Component
                        </Typography>
                            <Button color="inherit">Login</Button>
                    </Toolbar>
                </AppBar>
            </div>
        )
    }
}
export default withStyles(style)(AppBAr);
