import React from "react"
import Modal from "@material-ui/core/Modal"
import Typography from "@material-ui/core/Typography"
import Button from "@material-ui/core/Button"
import Icon from "@material-ui/core/Icon"
import Popover from "@material-ui/core/Popover"
import ClickAwayListener from "@material-ui/core/ClickAwayListener"


class ModalDemo extends React.Component{
    constructor(props){
        super(props);
        this.state={
            data:"",
            open:false,
            popover:false
        };
        this.onBUttonClick=this.onBUttonClick.bind(this);
        this.onCloseClick=this.onCloseClick.bind(this);
        this.onPopOverOpen=this.onPopOverOpen.bind(this);
        this.clickAwayEvent=this.clickAwayEvent.bind(this);
    }

    onBUttonClick(event){
        console.log(event);
        this.setState({
            open:true
        });
        console.log(this.state);
    }

    onCloseClick(event){
        console.log(event);
        this.setState({
            open:false
        });
    }

    onPopOverOpen(event){
        console.log(event);
        this.setState({
            popover:true
        });
    }

    clickAwayEvent(){
        this.setState({
            data:"",
            open:false,
            popover:false
        });
        console.log(this.state)
    }

    render(){
        return (
            <div>
                <Typography>Welcome To Modal</Typography>
                <Button variant={"outlined"} color={"primary"} size={"small"}
                      fullWidth={true}  onClick={this.onBUttonClick}><Icon>face</Icon></Button>
            <Modal open={this.state.open} >
                <div>
                <Button variant={"outlined"} color={"primary"} size={"small"}
                        fullWidth={true}  onClick={this.onCloseClick}>Base Component</Button>
                    <Typography>Welcome To Modal</Typography>
                </div>
            </Modal>
                <Button variant={"outlined"} color={"primary"} size={"small"}
                        fullWidth={false}  onClick={this.onPopOverOpen}>PopOver Button</Button>
                <Popover open={this.state.popover} transformOrigin={{
                    vertical:'top',
                    horizondal:'bottom',
                 }} anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}>
               <Typography>Welcome To Modal</Typography>
                </Popover>
            </div>
        )
    }
}
export default ModalDemo
