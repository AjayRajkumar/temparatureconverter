import  React from "react"
import  Avatar from "@material-ui/core/Avatar"
import Badge from "@material-ui/core/Badge"
import  {withStyles} from "@material-ui/core/styles"
import Chip from "@material-ui/core/Chip"
import Icon from "@material-ui/core/Icon"
import Card from "@material-ui/core/Card"
import CardHeader from "@material-ui/core/CardHeader"
import CardContent from "@material-ui/core/CardContent"
import CardActions from "@material-ui/core/CardActions"
import CardMedia from "@material-ui/core/CardMedia"
import Typography from "@material-ui/core/Typography"
import Button from "@material-ui/core/Button"

//
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import Divider from "@material-ui/core/Divider"

import Drawer from "@material-ui/core/Drawer"
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer"


const style=(theme)=>({
    root:{
        display:'flex',
        justifyContent:'center'
    },
    margin:{
        margin:10,
        backgroundColor:"green",
        color:"white"
    },
    chipStyle:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-around',
    },
    cardStyle:{
        maxWidth:500,
        margin:50,
        border:2,
    },
    content:{
        display:'flex',
        flexDirection:'row',
        flexWrap:'wrap'
    },
    alignRight:{
        alignRight:'true'
    },
    list:{
        maxWidth:400,
        backgroundColor: theme.palette.background.paper
    }

});

class AvatarDemo extends React.Component{
    constructor(props){
        super(props)
    }

    click(event){
        alert("You hav  e Clicked the",event)
    }

    render(){
        console.log(this.props.children);
        return(
            <div>
                <Drawer  variant={"permanent"}>
                    <List>
                    <ListItem>
                        <ListItemText primary={"DAtga"}>
                        </ListItemText>
                    </ListItem>
                    </List>
                    <Divider />
                    <List>
                        <ListItem>
                            <ListItemText primary={"DAtga"}>

                            </ListItemText>
                        </ListItem>

                    </List>
                </Drawer>
            </div>
        )
    }
}
export default withStyles(style)(Drawer)
