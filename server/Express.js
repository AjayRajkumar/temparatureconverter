var express=require('express');
var {ApolloServer,gql,ApolloError,AuthenticationError,ForbiddenError,PubSub}=require('apollo-server');
var {registerServer}=require('apollo-server-express');
//import {graphqlExpress,gr} from "apollo-server-express/src/expressApollo"
var faker=require('faker');
var bodyParser=require('body-parser');

var typeDefs=gql`

    type Query{
        human:[Human]
        newHuman:String
        work:workLife
        array:Array
    }

    type Mutation {
        addArrayValue(arr:Int):Int
    }


    type Array{
        number:Int
    }

    type Subscription {
        newMessage: String
    }

    type company{
        department:String
        positionName:String
    }

    type sideProjects{
        shortTime:String,
        logTimeProject:String
    }

    union workLife=company | sideProjects

    type Human{
        name: String!
        age:Int!,
        gender(gender:Gender="Male"):Gender,
        education:Education
    }

    type  Education{
        school:SchoolInfo
        college:CollegeInfo
        others:OtherInfo
    }


    type SchoolInfo {
        schoolName:String!
        state:String
        district:String
        grade:Int
        level:Level
        review:[Review]
    }

    type CollegeInfo {
        collegeName:String!
        state:String
        district:String
        grade:Int
        level:Level
        review:[Review]

    }

    type OtherInfo {
        otherName:String!
        state:String
        district:String
        grade:Int
        level:Level
        review:[Review]

    }

    #    
    #   fragment otherInfo on SchoolInfo{
    #      other:String
    # }


    #    mutation createInfo{
    #createInfo(name:String):SchoolInfo
    #    }

    type Review{
        name:String
        description:String
    }

    enum Gender{
        MALE,
        FEMALE,
        OTHER
    }


    enum Level{
        ONE
        TWO
        THREE
    }

    input InputObjectData{
        key:String
        value:String
    }
`;


var education={
    school:schoolInfo,
    college:collegeInfo,
    other:otherInfo
};

var schoolInfo={
    schoolName:faker.name.firstName(),
    state:faker.address.state(),
    district:faker.address.city(),
    grade:faker.random.number(),
    level:level,
    review:reviewArray
};



var collegeInfo={
    collegeName:faker.name.firstName(),
    state:faker.address.state(),
    district:faker.address.city(),
    grade:faker.random.number(),
    level:level,
    review:reviewArray
};

var otherInfo={
    otherName:faker.name.firstName(),
    state:faker.address.state(),
    district:faker.address.city(),
    grade:faker.random.number(),
    level:level,
    review:reviewArray
};

var level='ONE';

var reviewArray=[];

for (i=0;i<50;i++){
    reviewArray.push({
        name:faker.lorem.text(),
        descriptionn:faker.lorem.sentences()
    });
}

var humanArray=[];

for (let i=0;i<=50;i++)
{
    var Human ={
        name: faker.random.word(),
        age:faker.random.number(),
        education:{
            school:{
                schoolName:faker.name.firstName(),
                state:faker.address.state(),
                district:faker.address.city(),
                grade:faker.random.number(),
                level:level,
                review:reviewArray
            },
            college:{
                collegeName:faker.name.firstName(),
                state:faker.address.state(),
                district:faker.address.city(),
                grade:faker.random.number(),
                level:level,
                review:reviewArray
            },
            other:{
                otherName:faker.name.firstName(),
                state:faker.address.state(),
                district:faker.address.city(),
                grade:faker.random.number(),
                level:level,
                review:reviewArray
            }
        }
    };
    humanArray.push(Human);
}

console.log(humanArray);


var company ={
    department:"Traine",
    positionName:"CEO"
};

var sideProjects={
    shortTime:"Short",
    logTimeProject:"Long"
};

var someThingChanged="SomeThing Changed Because of SomeThing";
var array=1;

var arrayType={
    number:array
};

var resolvers={
    Query: {
        human: () => humanArray,
        newHuman: () => {
            return "Ajay"
        },
        array:()=>{
            return arrayType;
        }
    },
    Subscription:{
        newMessage:{
            subscribe :()=>{
                pubSub.asyncIterator(someThingChanged);
            }
        }
    },
    Mutation:{
        addArrayValue(arr){
            array=array++;
            return arrayType;
        }
    }
    /**
     errorQuery:(_,_,contextt)=>{
            if (contextt.scope){
                throw AuthenticationError('Please Provide the Authendication');
            }
            if (contextt.scope!='ADMIN'){
                throw ForbiddenError('Please Provide the Authendication');
            }
            return "Hacker";
        }
     */
//    }
};


var mocks={
    Int :()=>2,
    String:()=>"Demo",
    Float:()=>55.8
};

var pubSub=new PubSub();
//pubSub.asyncIterator();


var server=new ApolloServer({typeDefs,resolvers,mocks});
var app=express();
//app.use('graphql',bodyParser.json(),graphqlExpress({schema:typeDefs}));
//app.get('graphiql',graphiqlExpress({endpoint:'/graphql'}));

app.get('/',(req,res)=>{
    res.send("Json")
});

server.applyMiddleware({server:server,app:app});
//registerServer();

app.listen(8500,()=>{
    console.log("Welcome World");
});

setInterval(()=>{
    pubSub.publish(someThingChanged, {
        newMessage: new Date().toString(),
    })
});
